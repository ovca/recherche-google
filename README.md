# Recherche Google

#Gherkin 

Feature: Recherche de mot cle

  Scenario: Lancer une recherche de mot cle sur Google

    Given Lancer le navigateur google chrome
    
    And Accepter les conditions d'utilisations
    
    When lutilisateur recherche le mot <Sapient>
    
    Then Le resultat de la recherche saffiche et contient <Sapient>
    
    And Selectionne le premier lien contenant le mot cle

# Buid and CI on Gitlab

Créer un compte GitLab
 Cocher la case creation du fichier "README.md"
 
crée une clé gpg via Gitbash depuis votre ordinateur et l'ajouter à votre compte

** Configuration Git

$ git config --global user.name "Entrer votre d'utilisateur"

$ git config --global user.name "Entrer votre adresse email"

** Push sur un projet existant

$ cd "Aller dans le dossier existant"

$ git init

"git clonne with https:" 

$ git clone https://gitlab.com/ovca/recherche-google.git

$ git remote add origin http://gitlab.com/ovca/recherche-google.git

$ git add .

$ git commit -m "mettre un commentaire"

$ git push -u origin master
