Feature: Recherche de mot cle

  Scenario: Lancer une recherche de mot cle sur Google
    Given Lancer le navigateur google chrome
    And Accepter les conditions d'utilisations
    When lutilisateur recherche le mot <Sapient>
    Then Le resultat de la recherche saffiche et contient <Sapient>
    And Selectionne le premier lien contenant le mot cle