'''
Created on 25 avr. 2021

@author: Charles OUATTARA
'''


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#from chromedriver_py import binary_path
#import chromedriver_binary

#driver = webdriver.Chrome()
driver = webdriver.Remote(
        command_executor="http://selenium__standalone-chrome")

#Scenario
# Lancer une recherche de mot cle sur Google

#Given
# Lancer le navigateur google chrome
driver.get("https://www.google.fr/")

#And
# Accepter les conditions d'utilisations
WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#zV9nZe > div')))
driver.find_element_by_css_selector('#zV9nZe > div').click()


#When
# lutilisateur recherche le mot <Sapient>
search = driver.find_element_by_css_selector('body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input')
search.click()
search.send_keys("Sapient")

search.send_keys(Keys.RETURN)

# Then
# Le resultat de la recherche saffiche et contient <Sapient>
WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#rso > div:nth-child(1) > div:nth-child(1) > div > div > div.yuRUbf > a > h3')))

# And Selectionne le premier lien contenant le mot cle
driver.find_element_by_css_selector('#rso > div:nth-child(1) > div:nth-child(1) > div > div > div.yuRUbf > a > h3').click()

driver.quit()
